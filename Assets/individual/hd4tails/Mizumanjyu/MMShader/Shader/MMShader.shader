﻿Shader "Custom/MizuShader_v1.1"
{
	Properties {
		_Color ("Color(in)", Color) = (1,1,1,1)					//内側の色
		_MainTex ("Texture(Color)(in)", 2D) = "white" {}		//内側のテクスチャ
		_Glossiness ("Glossiness(in)", Range(0,1)) = 0.2		//内側の光沢
		_Metallic ("Metallic(in)", Range(0,1)) = 0.0			//内側の金属光沢
		_NormalMap ("NormalMap(in)", 2D) = "bump" {}			//内側のノーマルマップ
		[Space(40)]
		_Color2 ("Color(out)", Color) = (1,1,1,1)				//外側の色
		_Glossiness2 ("Glossiness(out)", Range(0,1)) = 0.9		//外側の光沢
		_Metallic2 ("Metallic(out)", Range(0,1)) = 0.0			//外側の金属光沢
		_Tickness2 ("Tickness(out)", Range(0,1)) = 0.25			//外側の厚み
		[Space(20)]
		_NormalMap2 ("NormalMap(out)", 2D) = "bump" {}			//外側の表面の歪用ノーマルマップ
		_Distorted2 ("Distorted(out)", Range(0,1)) = 0.3		//外側の表面の歪の強さ
		_Speed2 ("Speed(out)", Range(0,1)) = 0.1				//外側の表面の歪の流れる速度
	}
	SubShader {
		LOD 200

		//--内側のレンダリング開始--//
			Tags{
				"RenderType"="Opaque"			//不透明描画
				"Queue"="Geometry"				//描画優先度
			}
			CGPROGRAM
				#pragma surface surf Standard fullforwardshadows	//surfaceシェーダー(Standard)使用
				#pragma target 3.0
			
				fixed4 _Color;			//内側の色
				sampler2D _MainTex;		//内側のテクスチャ
				half _Glossiness;		//内側の光沢
				half _Metallic;			//内側の金属光沢
				sampler2D _NormalMap;	//内側のノーマルマップ

				struct Input {
					float2 uv_MainTex;		//テクスチャのUV
					float2 uv_NormalMap;	//ノーマルマップのUV
				};


				void surf (Input IN, inout SurfaceOutputStandard o) {
					fixed4 colorTex = tex2D (_MainTex, IN.uv_MainTex) * _Color;
					float3 normalTex  = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));	//UnityでNormalMapは変換されているため、それをもとに戻す
				
					o.Albedo = colorTex.rgb;
					o.Alpha = colorTex.a;
					o.Smoothness = _Glossiness;
					o.Metallic = _Metallic;
					o.Normal = normalTex;
				}
			ENDCG
		//--内側のレンダリング終了--//

		GrabPass{}		//1パス出力→2パス入力

		//--外側のレンダリング開始--//
			Tags{
				"RenderType"="Transparent"		//透明描画
				"Queue"="Geometry+1"			//描画優先度　アバターに使用する場合は他のオブジェクトのQueueがGeometry+2以上である必要あり（以下だとそのオブジェクトが2重に映る）
			}
			CGPROGRAM
				#pragma surface surf Standard fullforwardshadows vertex:vert	//Surfaceシェーダー　vertexシェーダー使用
				#pragma target 3.0
			
				//--関数定義--//
				//カメラの回転マトリクス
				float4x4 getCameraRotationMat(){
					//カメラの右方向
					float3 cam_right = normalize(UNITY_MATRIX_V[0].xyz);
					//カメラの上方向
					float3 cam_up = normalize(UNITY_MATRIX_V[1].xyz);
					//カメラの前方向
					float3 cam_forward = -normalize(UNITY_MATRIX_V[2].xyz);
             
					float4x4 rotMat = float4x4(	cam_right,		0,
														cam_up,			0,
														cam_forward,	0,
														0, 0, 0,		1);
					return rotMat;
				}
				//ノーマルマップ方向への回転マトリクス（ノーマルマップ+別のノーマルを足すときにUVの継ぎ目が出ないようにするために必要）
				float4x4 getNormalMapRotationMat(float3 vec){
             
					float4x4 rotMat = float4x4(	vec.z,		vec.y,		vec.x,		0,	//0
												vec.y,		vec.x,		vec.y,		0,	//0
												vec.x,		vec.z,		vec.z,		0,	//1
													0,			0,			0,		1);
					return rotMat;
				}

				//原点から座標までの角度（0-360度→0-1）
				float getAngle(float2 xy) {
					const float pi = 3.141593;
						return 0.5 + atan2(xy.y, xy.x) / pi / 2;
				}
				//--関数定義ここまで--//

				struct Input {
					
					float4 screenPos;		//screen（カメラ画像）上での各点の座標
					float3 worldPos;		//world座標での各点の位置
					float2 uv_NormalMap;	//ノーマルマップのUV
					float3 wNormal;			//world座標での各点のNormal(vertexシェーダーに追加)
				};
			
				fixed4 _Color2;					//外側の色
				half _Glossiness2;				//外側の光沢
				half _Metallic2;				//外側の金属光沢
				half _Tickness2;				//外側の厚み
				half _Distorted2;				//外側の表面の歪の強さ
				half _Speed2;					//外側の表面の歪の流れる速度
			
				sampler2D _NormalMap;			//内側のノーマルマップ
				sampler2D _NormalMap2;			//外側の表面の歪用ノーマルマップ
				sampler2D _GrabTexture;			//1パス出力テクスチャ（カメラ出力）
				uniform float4 _NormalMap2_ST;	//外側の表面の歪用ノーマルマップのpan,size

				void vert(inout appdata_full v, out Input o){
					UNITY_INITIALIZE_OUTPUT(Input, o);			//定型文
					o.wNormal = normalize(UnityObjectToWorldNormal(v.normal));	//world座標の各点のノーマル
				}

				void surf (Input IN, inout SurfaceOutputStandard o) {
					//normalマップのUV計算
					float3 worldPos = IN.worldPos;		//world座標での各点の位置
					float3 worldNormal = IN.wNormal;	//world座標での各点のNormal
					float4 screenPos = IN.screenPos;	//screen（カメラ画像）上での各点の座標
					float3 normalTex  = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));		//内側のノーマルマップ
					float4x4 camRotMat = getCameraRotationMat();

					float4 worldCenterPos = mul( unity_ObjectToWorld, float4(0,0,0,1) );		//world座標でのオブジェクトの原点
					float3 worldObjPos = mul( unity_WorldToObject, float4(worldPos.xyz - worldCenterPos.xyz, 0) ).xyz;	//world座標でのオブジェクトの原点を中心とした各点の位置
					float2 distortedUV;			//歪用ノーマルマップ座標
						distortedUV.x = getAngle(worldObjPos.xz);	//Uは中心点から見たときの各点のxz方向の角度
						distortedUV.y = worldObjPos.y + _Time.x * 2 * _Speed2 + length(worldObjPos.xz) * (-0.5);	//Vはy方向に中心点から見たときの各点のxz方向の距離を足したものに時間を引いたもの
						distortedUV = float2(distortedUV.xy * _NormalMap2_ST.xy + _NormalMap2_ST.zw);				//tileの値で大きさを変える
					float3 distortedNormalTex = UnpackScaleNormal(tex2D(_NormalMap2, distortedUV),_Distorted2);			//各点の歪ノーマル

					float3 moveNormal = normalize(fixed3(worldNormal.xy + distortedNormalTex.xy, worldNormal.z * distortedNormalTex.z));	//1パス出力テクスチャをずらす方向のNormal
					float2 moveDiffUV = mul(camRotMat ,float4( moveNormal, 1.0 ) ).xy;	//カメラから見たときの1パス出力テクスチャをずらす距離

					screenPos.w += 0.0001;
					float2 grabUV = (screenPos.xy / screenPos.w);					//画面のテクスチャから色を取得するために、画面の位置をUV座標（0-1）に変換
					float dist = 1 / (distance(_WorldSpaceCameraPos, worldPos));	//カメラ位置からオブジェクト位置までの距離を計算
					float3 grabColor = tex2D(_GrabTexture, grabUV + moveDiffUV * dist * 0.2 * _Tickness2).rgb;	//最終的なテクスチャを取得
                    
				    
				    o.Emission = lerp(grabColor ,_Color2.rgb, pow(saturate(length(moveDiffUV)),2) * (float)_Color2.a);
					o.Metallic = _Metallic2;
					o.Smoothness = _Glossiness2;

					
					float4x4 normalMapRotMat = getNormalMapRotationMat(normalTex);
					o.Normal = normalize(mul(normalMapRotMat ,float4( distortedNormalTex, 0.0 ) ).xyz);
				}
			ENDCG
		//--外側のレンダリング終了--//
	}
	FallBack "Diffuse"
}